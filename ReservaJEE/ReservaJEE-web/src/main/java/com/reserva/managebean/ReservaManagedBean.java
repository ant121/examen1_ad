package com.reserva.managebean;

import com.reserva.entidades.Reserva;
import com.reserva.session.ReservaFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "reservaManagedBean")
@ViewScoped
public class ReservaManagedBean implements Serializable,ManagedBeanInterface<Reserva> {

    @EJB
    private ReservaFacadeLocal reservaFacadeLocal;

    //Una Lista para traer el listado de cargos
    private List<Reserva> listaReserva;

    //Objeto de tipo Cargo
    private Reserva reserva;

    //Constructor
    public ReservaManagedBean() {
    }

    //Paso 2
    @PostConstruct
    public void init() {
        //lista de los cargos que estan en la BDD
        listaReserva = reservaFacadeLocal.findAll();
    }

    @Override
    public void nuevo() {
        reserva = new Reserva();
    }

    @Override
    public void grabar() {
        try {
            if (reserva.getId() == null) {
                reservaFacadeLocal.create(reserva);
            } else {
                reservaFacadeLocal.edit(reserva);
            }

            reserva = null;

            listaReserva = reservaFacadeLocal.findAll();
            mostrarMensajeTry("Informacion OK", FacesMessage.SEVERITY_INFO);
        } catch (Exception e) {
            mostrarMensajeTry("Ocurrio un error", FacesMessage.SEVERITY_ERROR);
        }
    }

    @Override
    public void seleccionar(Reserva c) {
        reserva = c;
    }

    @Override
    public void eliminar(Reserva c) {
        try {
            reservaFacadeLocal.remove(c);

            listaReserva = reservaFacadeLocal.findAll();

            mostrarMensajeTry("Informacion OK", FacesMessage.SEVERITY_INFO);
        } catch (Exception e) {
            mostrarMensajeTry("Ocurrio un error", FacesMessage.SEVERITY_ERROR);
        }

    }

    @Override
    public void cancelar() {
        reserva = null;
        listaReserva = reservaFacadeLocal.findAll();
    }

    public List<Reserva> getListaReserva() {
        return listaReserva;
    }

    public void setListaReserva(List<Reserva> listaReserva) {
        this.listaReserva = listaReserva;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    
}
